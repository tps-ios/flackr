//
//  MasterViewController.swift
//  tp2
//
//  Created by mbdsa on 30/01/2018.
//  Copyright © 2018 mbdsa. All rights reserved.
//

import UIKit
import Foundation

class MasterViewController: UITableViewController {
    
    var i:Int = 0
    
    var detailViewController: DetailViewController? = nil
    var objects = [Any]()
    var resData: NSDictionary = NSDictionary()
    var items: [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationItem.leftBarButtonItem = self.editButtonItem
        
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(insertNewObject(_:)))
        self.navigationItem.rightBarButtonItem = addButton
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        
        // new data
        getJson();
    }
    
    private func getJson() {
        // Set the URL the request is being made to.
        let requestURL = NSURL(string: "http://api.flickr.com/services/feeds/photos_public.gne?tags=MOTS_CLES&tagmode=any&format=json&jsoncallback=?")!

        let request = NSMutableURLRequest(url: requestURL as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) { print(error!)
            } else {
                do {
                    var content = String.init(data: data!, encoding: .utf8)
                    let indexStartOfText = content!.index(content!.startIndex, offsetBy: 1)
                    let indexEndOfText = content!.index(content!.endIndex, offsetBy: -1)
                    content = String(content![indexStartOfText..<indexEndOfText])
                    
                    let data = content?.data(using: .utf8)
                    
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    {
                        self.items = jsonResult["items"] as! [NSDictionary]
                        self.resData = jsonResult
                        
                        for item in self.items {
                            let title:String! = (item ).value(forKey: "title") as? String
                            let author:String! = (item ).value(forKey: "author") as? String
                            
                            self.objects.insert(title.first as Any, at:0)
                        }
                        print(self.objects)
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            } })
        dataTask.resume()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func insertNewObject(_ sender: Any) {
        i += 1
        objects.insert("Titre " + String(i), at: 0)
        let indexPath = IndexPath(row: 0, section: 0)
        self.tableView.insertRows(at: [indexPath], with: .automatic)
    }
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let object = objects[indexPath.row] as! String
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }
    
    // MARK: - Table View
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let object = objects[indexPath.row] as! String
        cell.textLabel!.text = object.description
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            objects.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    
    
}

