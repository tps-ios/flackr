//
//  FlickerImageViewController.swift
//  flackr_RG
//
//  Created by GIANFALDONE Michael on 27/02/2018.
//  Copyright © 2018 ROSTAN GIANFALDONE. All rights reserved.
//

import UIKit

class FlickerImageViewController: UIViewController {
    
    //MARK: properties
    @IBOutlet weak var displayImage: UIImageView!
    @IBOutlet weak var imageTitle: UILabel!
    @IBOutlet weak var imageDesc: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        imageTitle.text = "Titre test";
        imageDesc.text = "desc";
        //https://www.royalcanin.fr/wp-content/uploads/Golden-Retriever-Images-Photos-Animal-000120.png
        let url = "http:\\/\\/farm5.staticflickr.com\\/4746\\/26638971228_eea01130cb_m.jpg";
        //"http:\\/\\/farm5.staticflickr.com\\/4746\\/26638971228_eea01130cb_m.jpg"
        let imgURL = url.replacingOccurrences(of: "\\", with: "");
        NSLog(imgURL);
        retrievesImageFromUrl(imageURL: imgURL)
        
    }
    
    private func retrievesImageFromUrl(imageURL: String) {
        
        let requestURL = NSURL(string: imageURL)!
        
        let request = NSMutableURLRequest(url: requestURL as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        
        //NSURL(string: imageURL)! as URL
        session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error!)
                return
            }
            
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.displayImage.image = image
            })
            
        }).resume()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
